package pubg_case.robin.vitalij.pubgcase.Model;

import android.graphics.Bitmap;

/**
 * Created by marco.granatiero on 03/02/2015.
 */
public class GameEntity_case {
    public Bitmap imageResId;
    public String titleResId;
    public int colorId;

    public GameEntity_case(Bitmap imageResId, String titleResId, int colorId){
        this.imageResId = imageResId;
        this.titleResId = titleResId;
        this.colorId = colorId;
    }
}
