package pubg_case.robin.vitalij.pubgcase.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.text.SimpleDateFormat;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.R;
import pubg_case.robin.vitalij.pubgcase.activity.MainActivity;
import pubg_case.robin.vitalij.pubgcase.presenter.IMainPresetnerMoneyImpl;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney2;

/**
 * Created by vital on 14.03.2018.
 */

public class Fragment_money extends Fragment implements View.OnClickListener, MainViewMoney, MainViewMoney2, RewardedVideoAdListener {
    private TableRow tableRowMoney;
    private TableRow tableRowAd;
    private IMainPresetnerMoneyImpl iMainPresetnerMoney;

    private SharedPreferences sPref;
    public static final String APP_PREFERENCES = "mysettings";

    private RewardedVideoAd mRewardedVideoAd;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_money, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.history_money));

        tableRowMoney = (TableRow)view.findViewById(R.id.tableRow1);
        tableRowAd = (TableRow)view.findViewById(R.id.tableRow2);

        tableRowAd.setOnClickListener(this);
        tableRowMoney.setOnClickListener(this);

        iMainPresetnerMoney = new IMainPresetnerMoneyImpl(((Location)getActivity().getApplication()).getDataBaseHelper(), this, this, getContext());

        sPref = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        MobileAds.initialize(getActivity(), getResources().getString(R.string.admod_id));

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(getActivity());
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.tableRow1:
                iMainPresetnerMoney.addMoney(sPref, APP_PREFERENCES);
                break;
            case R.id.tableRow2:
               // showRewardedVideo();
                iMainPresetnerMoney.Reclam();
                break;
        }
    }

    @Override
    public void getMoney(String money) {
        ((MainActivity)this.getActivity()).mTitle.setText(money);
    }

    @Override
    public void getDialog(String text) {
                Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void loadRewardedVideoAd() {
        if (!mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.loadAd(getResources().getString(R.string.admod_video), new AdRequest.Builder().build());
        }
    }

    @Override
    public void showRewardedVideo() {
        // mShowVideoButton.setVisibility(View.INVISIBLE);
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        } else
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.Reclam_info2), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        iMainPresetnerMoney.ReclamResult();
    }


    @Override
    public void onRewardedVideoAdLoaded() {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        loadRewardedVideoAd();
    }


    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }
}
