package pubg_case.robin.vitalij.pubgcase.presenter;

import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Utils.GenerateBD;
import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.presenter.interfac.IMainPres;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney;

/**
 * Created by vital on 12.03.2018.
 */

public class IMainPresImpl implements IMainPres {
    private MainViewMoney mainView;
    private DataBaseHelper myDbHelper;

    public IMainPresImpl(DataBaseHelper myDbHelper, MainViewMoney mainView) {
        this.myDbHelper = myDbHelper;
        this.mainView = mainView;
    }

    @Override
    public void getMoney() {
        mainView.getMoney(GetColor.getMoney(GenerateBD.getMoney(myDbHelper)));
    }
}
