package pubg_case.robin.vitalij.pubgcase.Model.inventory;

/**
 * Created by vital on 04.03.2018.
 */

public class Invent {
    private boolean aBoolean;
    private double price;
    private int ID_Inventory;

    public boolean isaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getID_Inventory() {
        return ID_Inventory;
    }

    public void setID_Inventory(int ID_Inventory) {
        this.ID_Inventory = ID_Inventory;
    }

    public Invent(double price, int ID_Inventory) {
        this.price = price;
        this.ID_Inventory = ID_Inventory;
        this.aBoolean = false;
    }
}
