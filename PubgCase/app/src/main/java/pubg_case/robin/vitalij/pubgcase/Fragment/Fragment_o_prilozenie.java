package pubg_case.robin.vitalij.pubgcase.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pubg_case.robin.vitalij.pubgcase.R;


/**
 * Created by vital on 19.07.2017.
 */

public class Fragment_o_prilozenie extends Fragment {

    private Button o_razrab;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_o_prilozenie, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.O_prilozenie_title));

        o_razrab = (Button) view.findViewById(R.id.o_razrab);

        o_razrab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction;
                Fragment_developer fragment_developer = new Fragment_developer();
                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_main, fragment_developer);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

    return view;
    }

}
