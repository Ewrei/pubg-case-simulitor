package pubg_case.robin.vitalij.pubgcase.presenter;

import android.os.Handler;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Model.Clothes;
import pubg_case.robin.vitalij.pubgcase.Utils.GenerateBD;
import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;
import pubg_case.robin.vitalij.pubgcase.presenter.interfac.IMainPresenter;
import pubg_case.robin.vitalij.pubgcase.Utils.DiscreteScrollViewOptions;
import pubg_case.robin.vitalij.pubgcase.view.MainView;

/**
 * Created by vital on 12.03.2018.
 */

public class IMainPresenterImpl implements IMainPresenter {
    private MainView mainView;
    private GenerateBD generateBD;

    public IMainPresenterImpl(DataBaseHelper myDbHelper, MainView mainView) {
        this.mainView = null;
        this.generateBD = new GenerateBD(myDbHelper);
        this.mainView = mainView;
    }

    public void getPokypka(Pubg_case pubg_case, DiscreteScrollView discreteScrollView){
        mainView.buttonL(false);
        int position = generateBD.openCase(pubg_case);
        if(position == -1 ) {
            mainView.openAlert();
        }else {
            Clothes clothes = pubg_case.getClothesList().get(position);
            mainView.getMoney(GetColor.getMoney(generateBD.getMoney2()));
            DiscreteScrollViewOptions.smoothScrollToUserSelectedPosition(discreteScrollView, position);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run() {
                    mainView.openAlert2(clothes.getColour(), clothes.getBitmap(), clothes.getName());
                    mainView.buttonL(true);
                }}, 1500);
        }
    }

}
