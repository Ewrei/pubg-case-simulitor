/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pubg_case.robin.vitalij.pubgcase.Fragment.fragment_information;

import android.annotation.SuppressLint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;
import pubg_case.robin.vitalij.pubgcase.R;

/**
 * A fragment for displaying an image.
 */
public class ImageFragment extends Fragment {

  private static final String KEY_IMAGE_RES = "com.google.samples.gridtopager.key.imageRes";


  public ImageFragment newInstance(int key) {
    ImageFragment fragment = new ImageFragment();
      Bundle argument = new Bundle();
      argument.putInt(KEY_IMAGE_RES, key);
      fragment.setArguments(argument);

    return fragment;
  }

  @SuppressLint("NewApi")
  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.fragment_image, container, false);
    Pubg_case pubg_case = ((Location)getActivity().getApplication()).getPubg_case();
    Bundle arguments = getArguments();
    int imageRes = arguments.getInt(KEY_IMAGE_RES);

      view.findViewById(R.id.image).setTransitionName(String.valueOf(R.drawable.animal_2024172));
      view.findViewById(R.id.image).setBackground(new BitmapDrawable(getResources(), pubg_case.getClothesList().get(imageRes).getBitmap()));

    TextView textView = (TextView) view.findViewById(R.id.textView4);
    TextView random_textView = (TextView)view.findViewById(R.id.random_textView);
    View view1 = (View)view.findViewById(R.id.view);

    textView.setText(pubg_case.getClothesList().get(imageRes).getName());
    random_textView.setText(String.valueOf(pubg_case.getClothesList().get(imageRes).getRandom() + "%"));
    view1.setBackgroundColor(GetColor.getColor(pubg_case.getClothesList().get(imageRes).getColour()));

    Glide.with(this)
        .load(null)
        .listener(new RequestListener<Drawable>() {
          @Override
          public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable>
              target, boolean isFirstResource) {
            // The postponeEnterTransition is called on the parent ImagePagerFragment, so the
            // startPostponedEnterTransition() should also be called on it to get the transition
            // going in case of a failure.
            getParentFragment().startPostponedEnterTransition();
            return false;
          }

          @Override
          public boolean onResourceReady(Drawable resource, Object model, Target<Drawable>
              target, DataSource dataSource, boolean isFirstResource) {
            // The postponeEnterTransition is called on the parent ImagePagerFragment, so the
            // startPostponedEnterTransition() should also be called on it to get the transition
            // going when the image is ready.
            getParentFragment().startPostponedEnterTransition();
            return false;
          }
        })
        .into((ImageView) view.findViewById(R.id.image));
    return view;
  }
}
