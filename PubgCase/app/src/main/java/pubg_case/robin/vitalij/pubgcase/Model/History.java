package pubg_case.robin.vitalij.pubgcase.Model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

/**
 * Created by vital on 05.03.2018.
 */

public class History {
    private double money;
    private double money_time;
    private int game_case;
    private int gray;
    private int gren;
    private int blue;
    private int purple;
    private int purple2;
    private int red;

    public History(double money, double money_time, int game_case, int gray, int gren, int blue, int purple, int purple2, int red) {
        this.money = money;
        this.money_time = money_time;
        this.game_case = game_case;
        this.gray = gray;
        this.gren = gren;
        this.blue = blue;
        this.purple = purple;
        this.purple2 = purple2;
        this.red = red;
    }

    public int getPurple2() {
        return purple2;
    }

    public String getMoneyString() {
       NumberFormat numberFormat = NumberFormat.getNumberInstance();
       numberFormat.setMaximumFractionDigits(2);
       numberFormat.setGroupingUsed(true);
       return numberFormat.format(money);
   }

    public String getMoney_timeString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(money_time);
    }

    public String getGame_caseString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(game_case);
    }


    public String getGrayString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(gray);
    }



    public String getGreenString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(gren);
    }

    public String getBlueString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(blue);
    }

    public String getPurpleString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(purple);
    }

    public String getPurple2String() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(purple2);
    }

    public String getRedString() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(red);
    }


    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getMoney_time() {
        return money_time;
    }

    public void setMoney_time(double money_time) {
        this.money_time = money_time;
    }

    public int getGame_case() {
        return game_case;
    }

    public void setGame_case(int game_case) {
        this.game_case = game_case;
    }

    public int getGray() {
        return gray;
    }

    public void setGray(int gray) {
        this.gray = gray;
    }

    public int getGren() {
        return gren;
    }

    public void setGren(int gren) {
        this.gren = gren;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public int getPurple() {
        return purple;
    }

    public void setPurple(int purple) {
        this.purple = purple;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }


}
