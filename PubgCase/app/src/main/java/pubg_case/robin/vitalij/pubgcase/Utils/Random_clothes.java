package pubg_case.robin.vitalij.pubgcase.Utils;

import pubg_case.robin.vitalij.pubgcase.Model.Clothes;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;

/**
 * Created by vital on 12.03.2018.
 */

public interface Random_clothes {
    Clothes getClothes(Pubg_case pubg_case);
    int getPosition();
    void setPosition(int position);

}
