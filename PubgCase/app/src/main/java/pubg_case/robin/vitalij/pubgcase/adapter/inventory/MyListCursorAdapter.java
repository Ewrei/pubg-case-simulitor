package pubg_case.robin.vitalij.pubgcase.adapter.inventory;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.Model.inventory.Invent;
import pubg_case.robin.vitalij.pubgcase.Model.inventory.MyListItem;
import pubg_case.robin.vitalij.pubgcase.R;

/**
 * Created by vital on 04.03.2018.
 */

public class MyListCursorAdapter extends CursorRecyclerViewAdapter<MyListCursorAdapter.ViewHolder>{

    private Context context;
    private List<Invent> inventList;

    public interface INotesAdapterCallback {
        void print(boolean visible, double price);
    }

    INotesAdapterCallback callback;

    public MyListCursorAdapter(Context context, Cursor cursor, INotesAdapterCallback callback){
        super(context,cursor);
        this.context = context;
        this.inventList = new ArrayList<>();
        this.callback = callback;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public TextView priceTextView;
        public ImageView imageView;
        public View view2;

        public ViewHolder(View view) {
            super(view);
            mTextView = view.findViewById(R.id.textView);
            priceTextView = view.findViewById(R.id.price);
            imageView = view.findViewById(R.id.imageView);
            view2 = view.findViewById(R.id.view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_view, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor, int position) {
        MyListItem myListItem = MyListItem.fromCursor(cursor, context);
        viewHolder.priceTextView.setText(String.valueOf(myListItem.getClothes().getPrice()));
        viewHolder.imageView.setImageBitmap(myListItem.getClothes().getBitmap());
        viewHolder.view2.setBackgroundColor(GetColor.getColor(myListItem.getClothes().getColour()));

        inventList.add(new Invent(myListItem.getClothes().getPrice(), myListItem.getID_Inventory()));

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                inventList.get(position).setaBoolean(!inventList.get(position).isaBoolean());

                if(inventList.get(position).isaBoolean())
                    viewHolder.itemView.setBackgroundColor(Color.LTGRAY);
                else
                    viewHolder.itemView.setBackgroundColor(Color.WHITE);

                buttonClick();
            }
        });

    }

    //Проверка событий и обработка кнопки
    void buttonClick(){
        int n = 0;
        double sum = 0;

        for(int i = 0; i <inventList.size(); i++) {
            if (inventList.get(i).isaBoolean() == true) {
                sum += inventList.get(i).getPrice();
                callback.print(true, sum);
                n = 1;
            }
        }
            if(n == 0) {
                callback.print(false, 0);
            }
    }

    public List<Invent> getInventList() {
        return inventList;
    }
}