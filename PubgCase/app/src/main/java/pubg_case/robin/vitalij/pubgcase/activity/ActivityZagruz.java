package pubg_case.robin.vitalij.pubgcase.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Utils.GenerateBD;
import pubg_case.robin.vitalij.pubgcase.R;

public class ActivityZagruz extends AppCompatActivity {

    private DataBaseHelper myDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zagruz);

        myDbHelper = GenerateBD.otkritieBD(this);

        ((Location)getApplication()).setDataBaseHelper(myDbHelper);
        ((Location) getApplication()).setPubg_caseList(GenerateBD.getBD(myDbHelper, this));


        Intent intent;
        intent = new Intent(ActivityZagruz.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
