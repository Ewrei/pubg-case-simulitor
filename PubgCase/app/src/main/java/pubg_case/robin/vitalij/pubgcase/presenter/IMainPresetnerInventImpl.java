package pubg_case.robin.vitalij.pubgcase.presenter;

import java.util.List;

import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Utils.GenerateBD;
import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.presenter.interfac.IMainPresetnerInvent;
import pubg_case.robin.vitalij.pubgcase.view.MainViewTwo;

/**
 * Created by vital on 12.03.2018.
 */

public class IMainPresetnerInventImpl implements IMainPresetnerInvent {
    private MainViewTwo mainViewTwo;
    private GenerateBD generateBD;

    public IMainPresetnerInventImpl(DataBaseHelper myDbHelper, MainViewTwo mainView) {
        this.mainViewTwo = mainView;
        this.generateBD = new GenerateBD(myDbHelper);
    }

    @Override
    public void getDelete(double price, List<String> stringList) {
        generateBD.getDelete(price,stringList);
        mainViewTwo.udapteList();
        mainViewTwo.getMoney(GetColor.getMoney(generateBD.getMoney2()));
    }
}
