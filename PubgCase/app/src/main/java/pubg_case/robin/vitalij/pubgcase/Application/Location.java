package pubg_case.robin.vitalij.pubgcase.Application;

import android.app.Application;

import java.util.List;

import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Model.Clothes;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;

/**
 * Created by vital on 01.03.2018.
 */

public class Location  extends Application{
    private DataBaseHelper dataBaseHelper;
    private Pubg_case pubg_case;
    private List<Pubg_case> pubg_caseList;

    private List<Clothes> clothesList; //Список всех вещей;
    public Pubg_case getPubg_case() {
        return pubg_case;
    }

    public void setPubg_case(Pubg_case pubg_case) {
        this.pubg_case = pubg_case;
    }

    public List<Pubg_case> getPubg_caseList() {
        return pubg_caseList;
    }

    public void setPubg_caseList(List<Pubg_case> pubg_caseList) {
        this.pubg_caseList = pubg_caseList;
    }

    public List<Clothes> getClothesList() {
        return clothesList;
    }

    public void setClothesList(List<Clothes> clothesList) {
        this.clothesList = clothesList;
    }

    public DataBaseHelper getDataBaseHelper() {
        return dataBaseHelper;
    }

    public void setDataBaseHelper(DataBaseHelper dataBaseHelper) {
        this.dataBaseHelper = dataBaseHelper;
    }
}
