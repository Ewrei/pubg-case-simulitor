package pubg_case.robin.vitalij.pubgcase.Fragment.fragment_case_open;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.yarolegovich.discretescrollview.DSVOrientation;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.Model.Clothes;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;
import pubg_case.robin.vitalij.pubgcase.R;
import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.activity.MainActivity;
import pubg_case.robin.vitalij.pubgcase.presenter.IMainPresenterImpl;
import pubg_case.robin.vitalij.pubgcase.adapter.ShopAdapter;
import pubg_case.robin.vitalij.pubgcase.view.MainView;

/**
 * Created by vital on 18.03.2018.
 */

public class Fragment_case_open extends Fragment implements MainView, View.OnClickListener {
    private Pubg_case pubg_case;
    private DiscreteScrollView itemPicker;
    private InfiniteScrollAdapter infiniteAdapter;

    private TextSwitcher mTitle;
    private Button button_click;
    private IMainPresenterImpl iMainPresenter;
    private ScrollStateChangeListener scrollStateChangeListener;
    private ShopAdapter shopAdapter;

    private boolean inProgress = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_case_open, container, false);

        iMainPresenter = new IMainPresenterImpl(((Location)getActivity().getApplication()).getDataBaseHelper(), this);


        mTitle = (TextSwitcher) view.findViewById(R.id.title);
        mTitle.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                TextView textView = (TextView) inflater.inflate(R.layout.item_title, null);
                textView.setTextColor(getResources().getColor(R.color.colorP2));
                return textView;
            }
        });
        Animation in = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_top);
        Animation out = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_bottom);
        mTitle.setInAnimation(in);
        mTitle.setOutAnimation(out);


        pubg_case = ((Location) getActivity().getApplication()).getPubg_case();
        shopAdapter = new ShopAdapter(pubg_case.getClothesList());

        itemPicker = (DiscreteScrollView) view.findViewById(R.id.item_picker);
        itemPicker.setOrientation(DSVOrientation.HORIZONTAL);
        infiniteAdapter = InfiniteScrollAdapter.wrap(shopAdapter);
        itemPicker.setAdapter(infiniteAdapter);
        itemPicker.setOffscreenItems(3);
        itemPicker.setSlideOnFling(true);
        itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());



        scrollStateChangeListener = new ScrollStateChangeListener() {
            @Override
            public void onScrollStart(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
                mTitle.setText("");
            }

            @Override
            public void onScrollEnd(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
                mTitle.setText(pubg_case.getClothesList().get(infiniteAdapter.getRealCurrentPosition()).getName());
            }

            @Override
            public void onScroll(float scrollPosition, int currentPosition, int newPosition, @Nullable RecyclerView.ViewHolder currentHolder, @Nullable RecyclerView.ViewHolder newCurrent) {
            }
        };
        itemPicker.addScrollStateChangeListener(scrollStateChangeListener);

        onItemChanged(pubg_case.getClothesList().get(0));

        button_click = (Button) view.findViewById(R.id.button_click);
        button_click.setText(getResources().getString(R.string.open) + "(" + pubg_case.getPrice() + "$)");

        button_click.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_click:
                iMainPresenter.getPokypka(pubg_case, itemPicker);
                break;
        }
    }



    private void onItemChanged(Clothes item) {
        mTitle.setText(item.getName());
    }

    @Override
    public void getMoney(String money) {
        ((MainActivity)this.getActivity()).mTitle.setText(money);
    }


    @Override
    public void openAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getResources().getString(R.string.information))
                //  .setMessage(Html.fromHtml(model.getToString()))
                .setMessage(getResources().getString(R.string.no_balans))
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void openAlert2(int color, Bitmap bitmap, String name) {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.polyzit),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                });

        TextView editText = (TextView) dialogView.findViewById(R.id.tvTime);
        TextView textName = (TextView) dialogView.findViewById(R.id.textName);
        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image);
        CardView cardView = (CardView) dialogView.findViewById(R.id.card);
        View view1 = (View) dialogView.findViewById(R.id.view);

        view1.setBackgroundColor(GetColor.getColor(color));

        cardView.setCardBackgroundColor(Color.GREEN);
        imageView.setImageBitmap(bitmap);
        editText.setText(getResources().getString(R.string.vi_polyzit));
        textName.setText(name);
        android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }



    @Override
    public void onTextVisible(String text) {
        mTitle.setText(text);
    }

    @Override
    public void onTextVisibleGone() {
        mTitle.setText("");
    }

    @Override
    public void buttonL(boolean vibor) {
        if (vibor)
            button_click.setOnClickListener(this);
        else
            button_click.setOnClickListener(null);
    }


    public interface ScrollStateChangeListener<T extends ShopAdapter.ViewHolder> extends DiscreteScrollView.ScrollStateChangeListener<RecyclerView.ViewHolder> {
        @Override
        void onScrollStart(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition);

        @Override
        void onScrollEnd(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition);

        @Override
        void onScroll(float scrollPosition, int currentPosition, int newPosition, @Nullable RecyclerView.ViewHolder currentHolder, @Nullable RecyclerView.ViewHolder newCurrent);
    }
}
