package pubg_case.robin.vitalij.pubgcase.Utils;

import pubg_case.robin.vitalij.pubgcase.Model.Clothes;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;

/**
 * Created by vital on 06.03.2018.
 */

public class Random_clothesImpl  implements Random_clothes{
    private int position;
    public  Clothes getClothes(Pubg_case pubg_case)
    {

        double a = Math.random() * 10000;
        double b = 0;
        double razniza = 0;
        for(int i = 0; i < pubg_case.getClothesList().size(); i++){
            razniza = b;
            b += (pubg_case.getClothesList().get(i).getRandom() * 100);
           // Log.e("Clothes2", String.valueOf((pubg_case.getClothesList().get(i).getRandom() * 100)));
            if(razniza <= a && a < b) {
                position = i;
                return pubg_case.getClothesList().get(i);
            }

        }
        return pubg_case.getClothesList().get(3);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


}
