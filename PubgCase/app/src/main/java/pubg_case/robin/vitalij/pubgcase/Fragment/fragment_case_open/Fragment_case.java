package pubg_case.robin.vitalij.pubgcase.Fragment.fragment_case_open;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.activity.MainActivity;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;
import pubg_case.robin.vitalij.pubgcase.R;
import pubg_case.robin.vitalij.pubgcase.adapter.RVAdapter;
import pubg_case.robin.vitalij.pubgcase.presenter.IMainPresImpl;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney;

/**
 * Created by vital on 01.03.2018.
 */

public class Fragment_case extends Fragment  implements MainViewMoney {
    private List<Pubg_case> pubg_cases;
    private IMainPresImpl iMainPres;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_case, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.history_case));

        pubg_cases = ((Location) getActivity().getApplication()).getPubg_caseList();
        iMainPres = new IMainPresImpl(((Location)getActivity().getApplication()).getDataBaseHelper(), this);
        iMainPres.getMoney();

        RecyclerView rv = (RecyclerView)view.findViewById(R.id.rv);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(llm);

        RVAdapter adapter = new RVAdapter(pubg_cases, getContext(), getActivity());
        rv.setAdapter(adapter);

        return view;

    }

    @Override
    public void getMoney(String money) {
        ((MainActivity)this.getActivity()).mTitle.setText(money);
    }
}
