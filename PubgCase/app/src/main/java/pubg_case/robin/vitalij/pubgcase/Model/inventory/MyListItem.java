package pubg_case.robin.vitalij.pubgcase.Model.inventory;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.List;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.Model.Clothes;

/**
 * Created by vital on 04.03.2018.
 */

public class MyListItem{
    private String name;
    private Clothes clothes;
    private int ID_Inventory;

    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }

    public MyListItem(String name) {
        this.name = name;
    }

    public Clothes getClothes() {
        return clothes;
    }

    public void setClothes(Clothes clothes) {
        this.clothes = clothes;
    }

    public MyListItem(String name, Clothes clothes) {
        this.name = name;
        this.clothes = clothes;
    }

    public MyListItem(String name, Clothes clothes, int ID_Inventory) {
        this.name = name;
        this.clothes = clothes;
        this.ID_Inventory = ID_Inventory;
    }

    public int getID_Inventory() {
        return ID_Inventory;
    }

    public void setID_Inventory(int ID_Inventory) {
        this.ID_Inventory = ID_Inventory;
    }

    public static MyListItem fromCursor(Cursor cursor, Context activity) {
        //TODO return your MyListItem from cursor.
        List<Clothes> clothesList = ((Location)activity.getApplicationContext()).getClothesList();

        int curs = cursor.getInt(1);

        Log.e("CVOTRIM", String.valueOf(cursor.getInt(0) + "    " +  cursor.getInt(1)));
        for (int i = 0; i < clothesList.size(); i++)
            if(clothesList.get(i).getID_Clothes() == curs)
                return new MyListItem(String.valueOf(curs), clothesList.get(i), cursor.getInt(0));

        return  new MyListItem(cursor.getString(1));
    }
}
