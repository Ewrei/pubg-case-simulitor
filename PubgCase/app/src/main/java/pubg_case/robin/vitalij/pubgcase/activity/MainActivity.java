package pubg_case.robin.vitalij.pubgcase.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.Fragment.fragment_case_open.Fragment_case;
import pubg_case.robin.vitalij.pubgcase.Fragment.Fragment_history;
import pubg_case.robin.vitalij.pubgcase.Fragment.Fragment_information_case;
import pubg_case.robin.vitalij.pubgcase.Fragment.Fragment_o_prilozenie;
import pubg_case.robin.vitalij.pubgcase.Fragment.fragment_inventory.Fragment_inventory;
import pubg_case.robin.vitalij.pubgcase.Fragment.Fragment_money;
import pubg_case.robin.vitalij.pubgcase.R;
import pubg_case.robin.vitalij.pubgcase.Utils.InterstitialAd_ad;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainViewMoney {

    private static long back_pressed;
    public static int currentPosition;

    private android.support.v4.app.FragmentManager myFragmentManager;
    private FragmentTransaction fragmentTransaction;
    private Fragment_case fragment_case;
    private Fragment_information_case fragment_information_case;
    private Fragment_inventory fragment_inventory;
    private Fragment_history fragment_history;
    private Fragment_o_prilozenie fragment_o_prilozenie;
    private Fragment_money fragment_money;

    public TextView mTitle;

    //Рекламный блок
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("0");

        fragment_case = new Fragment_case();
        fragment_information_case = new Fragment_information_case();
        fragment_inventory = new Fragment_inventory();
        fragment_history = new Fragment_history();
        fragment_o_prilozenie = new Fragment_o_prilozenie();
        fragment_money = new Fragment_money();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        myFragmentManager = this.getSupportFragmentManager();

        if (savedInstanceState == null) {
            // при первом запуске программы
            fragmentTransaction = myFragmentManager
                    .beginTransaction();
            fragmentTransaction.add(R.id.content_main, fragment_case);
            fragmentTransaction.commit();
        }

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this, getResources().getString(R.string.admod_id));

        // Create the InterstitialAd_ad and set the adUnitId.
        mInterstitialAd = new InterstitialAd(this);
        // Defined in res/values/strings.xml
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admod_itteractive));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                InterstitialAd_ad.startGame(mInterstitialAd);
            }
        });

        InterstitialAd_ad.startGame(mInterstitialAd);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Log.e("TESTIM", "TRE");
        InterstitialAd_ad.showInterstitial(mInterstitialAd);

        if (id == R.id.nav_camera) {
            fragmentTransaction = myFragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.content_main, fragment_case);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            fragmentTransaction = myFragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.content_main, fragment_inventory);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_slideshow) {
            fragmentTransaction = myFragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.content_main, fragment_information_case);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_manage) {
            fragmentTransaction = myFragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.content_main, fragment_history);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else if(id == R.id.nav_manage2){
            fragmentTransaction = myFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_main, fragment_money);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_share) {
            fragmentTransaction = myFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_main, fragment_o_prilozenie);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_send) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Информация")
                    //  .setMessage(Html.fromHtml(model.getToString()))
                    .setMessage("Выйти из приложения?")
                    .setCancelable(false)
                    .setPositiveButton("Нет",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            })
                    .setNegativeButton("ОК",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finishAffinity();
                                }
                            });

            AlertDialog alert = builder.create();
            alert.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        ((Location)getApplication()).getDataBaseHelper().close(); //закрытие базы данных
        super.onDestroy();
    }

    @Override
    public void getMoney(String money) {
        mTitle.setText(money);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(myFragmentManager.getBackStackEntryCount() > 0)
                super.onBackPressed();
            else {
                if (back_pressed + 2000 > System.currentTimeMillis()) {
                    super.onBackPressed();
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.Exit_click), Toast.LENGTH_SHORT).show();
                }

                back_pressed = System.currentTimeMillis();
            }
        }
    }

}
