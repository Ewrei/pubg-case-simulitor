package pubg_case.robin.vitalij.pubgcase.Fragment.fragment_inventory;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Model.inventory.Invent;
import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.activity.MainActivity;
import pubg_case.robin.vitalij.pubgcase.R;
import pubg_case.robin.vitalij.pubgcase.adapter.inventory.MyListCursorAdapter;
import pubg_case.robin.vitalij.pubgcase.presenter.IMainPresetnerInventImpl;
import pubg_case.robin.vitalij.pubgcase.view.MainViewTwo;

/**
 * Created by vital on 03.03.2018.
 */

public class Fragment_inventory extends Fragment implements MyListCursorAdapter.INotesAdapterCallback, MainViewTwo{
    private DataBaseHelper myDbHelper;
    private GridLayoutManager mLayoutManager;

    public static final int MAX_SPAN_COUNT = 2;

    private  RecyclerView recyclerView;
    private MyListCursorAdapter myListCursorAdapter;

    private Button button;
    private double price;

    private IMainPresetnerInventImpl iMainPresetnerInvent;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inventory, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.history_inventory));

        myDbHelper = ((Location)getActivity().getApplication()).getDataBaseHelper();

        iMainPresetnerInvent = new IMainPresetnerInventImpl(myDbHelper, this);

        price = 0;


        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        Cursor cursor = null;

        button = (Button) view.findViewById(R.id.button);

        cursor = myDbHelper.query("Inventory", null, "", new String[]{}, null, null, null);
        myListCursorAdapter = new MyListCursorAdapter(getContext(), cursor, this);

        recyclerView.setAdapter(myListCursorAdapter);

        mLayoutManager = new GridLayoutManager(getActivity(), MAX_SPAN_COUNT);
        mLayoutManager.setSpanCount(3);
        recyclerView.setLayoutManager(mLayoutManager);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<Invent> inventList = myListCursorAdapter.getInventList();

                List<String> stringList = new ArrayList<>();
                for (int i = 0; i < inventList.size(); i++)
                    if (inventList.get(i).isaBoolean() == true) {
                        stringList.add(String.valueOf(inventList.get(i).getID_Inventory()));
                    }

                iMainPresetnerInvent.getDelete(price, stringList);
                print(false, 0);
            }
        });

        return view;
    }


    @Override
    public void print(boolean visible, double price) {
        if(visible) {
            button.setVisibility(View.VISIBLE);
            button.setText(getResources().getString(R.string.button_prodaza) + '(' + GetColor.getMoney(price) + ")");
            this.price = price;
        }
        else {
            button.setVisibility(View.GONE);
            this.price = 0;

        }
    }

    @Override
    public void getMoney(String money) {
        ((MainActivity)this.getActivity()).mTitle.setText(money);
    }

    @Override
    public void udapteList() {
        Cursor cursor  = null;
        cursor = myDbHelper.query("Inventory", null, "",  new String[] {}, null, null, null);
        myListCursorAdapter = new MyListCursorAdapter(getContext(), cursor, this);
        recyclerView.setAdapter( myListCursorAdapter );
        myListCursorAdapter.notifyDataSetChanged();
    }
}
