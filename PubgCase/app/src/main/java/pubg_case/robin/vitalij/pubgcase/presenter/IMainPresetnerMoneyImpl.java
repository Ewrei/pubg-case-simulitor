package pubg_case.robin.vitalij.pubgcase.presenter;

import android.content.Context;
import android.content.SharedPreferences;


import java.util.Calendar;
import java.util.Date;

import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Utils.GenerateBD;
import pubg_case.robin.vitalij.pubgcase.Utils.GetColor;
import pubg_case.robin.vitalij.pubgcase.R;
import pubg_case.robin.vitalij.pubgcase.presenter.interfac.IMainPresetnerMoney;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney2;

/**
 * Created by vital on 14.03.2018.
 */

public class IMainPresetnerMoneyImpl implements IMainPresetnerMoney {
    private MainViewMoney mainViewMoney;
    private MainViewMoney2 mainViewMoney2;
    private Context context;
    private GenerateBD generateBD;

    public IMainPresetnerMoneyImpl(DataBaseHelper myDbHelper, MainViewMoney mainViewMoney, MainViewMoney2 mainViewMoney2, Context context) {
        this.mainViewMoney = mainViewMoney;
        this.mainViewMoney2 = mainViewMoney2;
        this.context = context;
        this.generateBD =  new GenerateBD(myDbHelper);
    }

    @Override
    public void addMoney(SharedPreferences sPref, String APP_PREFERENCES_DATE) {
        long OBNOVLENIE = sPref.getLong(APP_PREFERENCES_DATE, 0);

        Date date_time = new Date();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date_time);

        if(OBNOVLENIE > date_time.getTime()) {
            mainViewMoney2.getDialog(context.getResources().getString(R.string.bonus_toas));
        }else {

            if (generateBD.moneyProverka()) {
                mainViewMoney2.getDialog(context.getResources().getString(R.string.money_toas));
            } else {
                instance.add(Calendar.HOUR, 3);// прибавляем 3 часа
                Date newDate = instance.getTime(); // получаем измененную дату
                SharedPreferences.Editor editor = sPref.edit();
                editor.putLong(APP_PREFERENCES_DATE, newDate.getTime());
                editor.commit();

                mainViewMoney.getMoney(GetColor.getMoney(generateBD.getMoney2()));
                mainViewMoney2.getDialog(context.getResources().getString(R.string.text_chet));
            }
        }

    }

    @Override
    public void Reclam() {
            mainViewMoney2.showRewardedVideo();
    }

    @Override
    public void ReclamResult() {

        generateBD.addMoney(100);
        mainViewMoney.getMoney(GetColor.getMoney(generateBD.getMoney2()));
        mainViewMoney2.getDialog(context.getResources().getString(R.string.text_chet));
    }
}
