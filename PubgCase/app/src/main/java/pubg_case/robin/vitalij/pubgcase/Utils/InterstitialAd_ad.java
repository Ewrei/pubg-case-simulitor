package pubg_case.robin.vitalij.pubgcase.Utils;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by vital on 15.10.2017.
 */

public class InterstitialAd_ad {


    public static void showInterstitial(InterstitialAd mInterstitialAd) {
        // Show the ad if it's ready. Otherwise toast and restart the game.
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            //  Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
            startGame(mInterstitialAd);
        }
    }

    public static void startGame(InterstitialAd mInterstitialAd) {
        // Request a new ad if one isn't already loaded, hide the button, and kick off the timer.
        if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            mInterstitialAd.loadAd(adRequest);
        }
    }
}
