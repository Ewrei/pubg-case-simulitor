package pubg_case.robin.vitalij.pubgcase.view;

/**
 * Created by vital on 14.03.2018.
 */

public interface MainViewMoney2 {
    public void getDialog(String text);
    public void showRewardedVideo();
    public void loadRewardedVideoAd();
}
