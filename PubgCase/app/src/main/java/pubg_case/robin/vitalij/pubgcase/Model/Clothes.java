package pubg_case.robin.vitalij.pubgcase.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;


/**
 * Created by vital on 01.03.2018.
 */

public class Clothes {
    private String Name;
    private double Price;
    private byte[] Image;
    private int Colour;
    private double Random;
    private int ID_Clothes;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public byte[] getImage() {
        return Image;
    }

    public void setImage(byte[] image) {
        Image = image;
    }

    public int getColour() {
        return Colour;
    }

    public void setColour(int colour) {
        Colour = colour;
    }

    public double getRandom() {
        return Random;
    }

    public void setRandom(double random) {
        Random = random;
    }

    public Bitmap getBitmap()
    {
        ByteArrayInputStream imageStream = new ByteArrayInputStream(getImage());
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
        return theImage;
    }



    public Clothes(String name, double price, byte[] image, int colour, double random, int ID_Clothes) {
        Name = name;
        Price = price;
        Image = image;
        Colour = colour;
        Random = random;
        this.ID_Clothes = ID_Clothes;
    }

    public int getID_Clothes() {
        return ID_Clothes;
    }

    public void setID_Clothes(int ID_Clothes) {
        this.ID_Clothes = ID_Clothes;
    }
}
