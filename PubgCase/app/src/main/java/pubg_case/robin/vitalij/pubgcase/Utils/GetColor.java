package pubg_case.robin.vitalij.pubgcase.Utils;

import android.graphics.Color;

import java.text.NumberFormat;

/**
 * Created by vital on 12.03.2018.
 */

public class GetColor {
    public static int getColor(int chislo) {
        switch (chislo){
            case 0:
                return Color.RED;
            case 1:
               return Color.parseColor("#EE82EE");
            case 2:
                return Color.parseColor("#800080");
            case 3:
                return Color.BLUE;
            case 4:
                return Color.GREEN;
            case 5:
                return Color.GRAY;
        }
        return 0;
    }


    public static String getMoney(double money) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setGroupingUsed(true);
        return numberFormat.format(money) + "$";

    }
}
