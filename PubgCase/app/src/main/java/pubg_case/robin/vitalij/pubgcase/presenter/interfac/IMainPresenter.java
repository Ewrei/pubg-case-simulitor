package pubg_case.robin.vitalij.pubgcase.presenter.interfac;

import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;
import pubg_case.robin.vitalij.pubgcase.view.MainView;

/**
 * Created by vital on 11.03.2018.
 */

public interface IMainPresenter {
    public void getPokypka(Pubg_case pubg_case, DiscreteScrollView discreteScrollView);
}
