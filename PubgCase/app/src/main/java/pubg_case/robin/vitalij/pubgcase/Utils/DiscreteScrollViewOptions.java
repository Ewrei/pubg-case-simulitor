package pubg_case.robin.vitalij.pubgcase.Utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;

import java.lang.ref.WeakReference;

import pubg_case.robin.vitalij.pubgcase.R;

/**
 * Created by yarolegovich on 08.03.2017.
 */

public class DiscreteScrollViewOptions {


    public static void smoothScrollToUserSelectedPosition(final DiscreteScrollView scrollView, int position){
        final RecyclerView.Adapter adapter = scrollView.getAdapter();

        int destination = Integer.parseInt(String.valueOf(position));
        if (adapter instanceof InfiniteScrollAdapter) {
            destination = ((InfiniteScrollAdapter) adapter).getClosestPosition(destination);
        }
        scrollView.smoothScrollToPosition(destination);

    }


}
