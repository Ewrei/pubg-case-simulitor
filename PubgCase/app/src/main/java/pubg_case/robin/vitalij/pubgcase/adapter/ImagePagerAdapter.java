/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pubg_case.robin.vitalij.pubgcase.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;


import pubg_case.robin.vitalij.pubgcase.Fragment.fragment_information.ImageFragment;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;


public class ImagePagerAdapter extends FragmentStatePagerAdapter {

  private Pubg_case pubg_case;

  public ImagePagerAdapter(Fragment fragment, Pubg_case pubg_case) {
    // Note: Initialize with the child fragment manager.
    super(fragment.getChildFragmentManager());
    this.pubg_case = pubg_case;
  }

  @Override
  public int getCount() {
    return pubg_case.getClothesList().size();
  }

  @Override
  public Fragment getItem(int position) {
    return new ImageFragment().newInstance(position);
  }
}
