package pubg_case.robin.vitalij.pubgcase.view;

import android.graphics.Bitmap;
import android.view.MotionEvent;

/**
 * Created by vital on 12.03.2018.
 */

public interface MainView {

    void getMoney(String money);
    void openAlert();
    void openAlert2(int color, Bitmap bitmap, String name);
    void onTextVisible(String text);
    void onTextVisibleGone();
    void buttonL(boolean vibor);
}
