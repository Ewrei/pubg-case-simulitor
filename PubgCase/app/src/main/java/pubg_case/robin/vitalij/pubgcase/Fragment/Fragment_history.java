package pubg_case.robin.vitalij.pubgcase.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Utils.GenerateBD;
import pubg_case.robin.vitalij.pubgcase.Model.History;
import pubg_case.robin.vitalij.pubgcase.R;

/**
 * Created by vital on 05.03.2018.
 */

public class Fragment_history extends Fragment {
    private DataBaseHelper myDbHelper;
    private History history;

    private TextView balans;
    private TextView balans_time;
    private TextView game_case;
    private TextView gray;
    private TextView gren;
    private TextView blue;
    private TextView purple;
    private TextView purple2;
    private TextView red;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.history_title));

        balans = (TextView) view.findViewById(R.id.balans);
        balans_time = (TextView)view.findViewById(R.id.balans_time);
        game_case = (TextView)view.findViewById(R.id.case_game);
        gray = (TextView)view.findViewById(R.id.gray);
        gren = (TextView)view.findViewById(R.id.gren);
        blue = (TextView)view.findViewById(R.id.blue);
        purple = (TextView)view.findViewById(R.id.purple);
        purple2 = (TextView)view.findViewById(R.id.purple2);
        red = (TextView)view.findViewById(R.id.red);

        myDbHelper  = ((Location)getActivity().getApplication()).getDataBaseHelper();

        history = GenerateBD.getHistory(myDbHelper);

        balans.setText(history.getMoneyString() + "$");
        balans_time.setText(history.getMoney_timeString() + "$");
        game_case.setText(history.getGame_caseString());
        gray.setText(history.getGrayString());
        gren.setText(history.getGreenString());
        blue.setText(history.getBlueString());
        purple.setText(history.getPurpleString());
        purple2.setText(history.getPurple2String());
        red.setText(history.getRedString());

        return view;
    }

}
