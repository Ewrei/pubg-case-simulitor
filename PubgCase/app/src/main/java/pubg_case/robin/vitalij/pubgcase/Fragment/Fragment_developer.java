package pubg_case.robin.vitalij.pubgcase.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import pubg_case.robin.vitalij.pubgcase.R;
import pubg_case.robin.vitalij.pubgcase.activity.MainActivity;
import pubg_case.robin.vitalij.pubgcase.view.MainViewMoney;

/**
 * Created by vital on 15.12.2017.
 */

public class Fragment_developer extends Fragment implements View.OnClickListener {

    private LinearLayout google_plus;
    private LinearLayout google_play;
    private LinearLayout facebook;
    private LinearLayout vk;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_developer, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.o_razrabe));

        google_plus = (LinearLayout) view.findViewById(R.id.google_plus);
        google_play = (LinearLayout) view.findViewById(R.id.google_play);
        facebook = (LinearLayout) view.findViewById(R.id.facebook);
        vk = (LinearLayout) view.findViewById(R.id.vk);

        google_plus.setOnClickListener(this);
        google_play.setOnClickListener(this);
        facebook.setOnClickListener(this);
        vk.setOnClickListener(this);



        return view;
    }

    @Override
    public void onClick(View view) {
        String url = "";
        switch (view.getId()) {
            case R.id.google_plus:
                url = "https://plus.google.com/communities/114718772612638784322";
                break;
            case R.id.google_play:
                url = "https://play.google.com/store/apps/developer?id=%D0%92%D0%B8%D1%82%D0%B0%D0%BB%D0%B8%D0%B9%20%D0%A0%D0%BE%D0%B1%D0%B8%D0%BD%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9";
                break;
            case R.id.vk:
                url = "https://vk.com/robinovsky";
                break;
            case R.id.facebook:
                url = "https://www.facebook.com/vitalij.robin.one";
                break;
        }
        Uri address = Uri.parse(url);
        Intent openlinkIntent = new Intent(Intent.ACTION_VIEW, address);
        startActivity(openlinkIntent);
    }

}
