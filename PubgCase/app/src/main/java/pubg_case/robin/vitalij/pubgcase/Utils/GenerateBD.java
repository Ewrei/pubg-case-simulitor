package pubg_case.robin.vitalij.pubgcase.Utils;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pubg_case.robin.vitalij.pubgcase.Application.Location;
import pubg_case.robin.vitalij.pubgcase.BD.DataBaseHelper;
import pubg_case.robin.vitalij.pubgcase.Model.Clothes;
import pubg_case.robin.vitalij.pubgcase.Model.History;
import pubg_case.robin.vitalij.pubgcase.Model.Pubg_case;

/**
 * Created by vital on 01.03.2018.
 */

public class GenerateBD {
    private DataBaseHelper myDbHelper;


    public GenerateBD(DataBaseHelper myDbHelper) {
        this.myDbHelper = myDbHelper;
    }

    public static DataBaseHelper  otkritieBD(Activity activity)
    {
        DataBaseHelper myDbHelper = new DataBaseHelper(activity);
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            try {
                myDbHelper.openDataBase();
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException sqle) {
            try {
                throw sqle;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return myDbHelper;
    }

    //Получение кейсов
    public static List<Pubg_case> getBD(DataBaseHelper dataBaseHelper, FragmentActivity activity){

        List<Pubg_case> pubg_caseList = new ArrayList<>();
        List<Clothes> clothesList = new ArrayList<>();

        Cursor cursor  = null;

        cursor = dataBaseHelper.query("Pubg_case    ", null, "",  new String[] {}, null, null, null);

        Log.e("Cursor", String.valueOf(cursor.getCount()));
        while (cursor.moveToNext()) {
            Pubg_case pubg_case = new Pubg_case();

            int id_pubg_case = cursor.getInt(0);

            pubg_case.setName(cursor.getString(1));
            pubg_case.setImageByteArray(cursor.getBlob(4));
            pubg_case.setPrice(cursor.getDouble(2));

            List<Clothes>clothes = getClothes(dataBaseHelper,  id_pubg_case);

            pubg_case.setClothesList(clothes);

            clothesList.addAll(clothes);
            pubg_caseList.add(pubg_case);

        }

        ((Location)activity.getApplication()).setClothesList(clothesList);
        cursor.close();
        return pubg_caseList;
    }

    private static List<Clothes> getClothes(DataBaseHelper dataBaseHelper, int id_pubg_case){
        List<Clothes> clothesList = new ArrayList<Clothes>();
        Cursor cursor  = null;

        cursor = dataBaseHelper.query("Сlothes", null, "ID_Case = ?", new String[]{Integer.toString(id_pubg_case)}, null, null, null);
        Log.e("Cursor2", String.valueOf(cursor.getCount()));
        while (cursor.moveToNext()) {
            Clothes clothes = new Clothes(cursor.getString(2), cursor.getDouble(3), cursor.getBlob(7), cursor.getInt(5), cursor.getDouble(6), cursor.getInt(0));
            clothesList.add(clothes);
        }

        cursor.close();
        return clothesList;
    }

    public static double getMoney(DataBaseHelper dataBaseHelper){
        Cursor cursor  = null;
        double money = 0;

        cursor = dataBaseHelper.query("History", null, "",  new String[] {}, null, null, null);
        while (cursor.moveToNext()) {
            money = cursor.getDouble(1);

        }
        cursor.close();
        return  money;
    }


    public static History getHistory(DataBaseHelper dataBaseHelper){
        Cursor cursor  = null;

        double money = 0;
        double money_time = 0;
        int game_case = 0;
        int gray = 0;
        int gren = 0;
        int blue = 0;
        int purple = 0;
        int purple2 = 0;
        int red = 0;

        cursor = dataBaseHelper.query("History", null, "",  new String[] {}, null, null, null);
        while (cursor.moveToNext()) {
            money = cursor.getDouble(1);
            game_case = cursor.getInt(2);
            gray = cursor.getInt(3);
            gren = cursor.getInt(4);
            blue = cursor.getInt(5);
            purple = cursor.getInt(6);
            red = cursor.getInt(7);
            money_time = cursor.getDouble(8);
            purple2 = cursor.getInt(9);
        }
        cursor.close();
        return  new History(money, money_time,game_case,gray,gren,blue,purple, purple2,red);
    }

    //метод для получение значений
    public  boolean moneyProverka(){
        double money = getMoney2();

        Cursor cursor2 = null;
        cursor2 = myDbHelper.query("Inventory", null, "",  new String[] {}, null, null, null);

        if(cursor2.getCount() > 3 || money > 2.5) {
            cursor2.close();
            return true;
        }else{
            addMoney(100);
            cursor2.close();
            return false;
        }
    }

    public  double getMoney2(){
        Cursor cursor  = null;
        double money = 0;

        cursor = myDbHelper.query("History", null, "",  new String[] {}, null, null, null);
        while (cursor.moveToNext()) {
            money = cursor.getDouble(1);

        }
        cursor.close();
        return  money;
    }


    public  void addMoney(double moneyAdd){
        //Метод обновления данных
        String sql = "UPDATE History SET money = money + " + moneyAdd + ", money_time = money_time + " + moneyAdd + " WHERE ID_History = 1";
        Cursor c =  myDbHelper.update(sql);
        c.moveToFirst();
        c.close();
    }


    public void getDelete(double price, List<String> stringList) {

        for(int i = 0; i < stringList.size(); i++)
            myDbHelper.delete("Inventory", "ID_Inventory=", stringList.get(i));


        addMoney(price);
    }


    public int openCase(Pubg_case pubg_case){

        double money = getMoney2();

        if (money < pubg_case.getPrice()) {
            return -1;
        }else {
            Random_clothesImpl random_clothesImpl = new Random_clothesImpl();
            Clothes clothes = random_clothesImpl.getClothes(pubg_case);

            ContentValues contentValues = new ContentValues();
            contentValues.put("ID_Clothes", clothes.getID_Clothes());
            myDbHelper.setInsert2("Inventory", null, contentValues);

            String stroka = null;
            switch (clothes.getColour()) {
                case 0:
                    stroka = "red = red + 1";
                    break;
                case 1:
                    stroka = "purple2 = purple2 + 1";
                    break;
                case 2:
                    stroka = "purple = purple + 1";
                    break;
                case 3:
                    stroka = "blue = blue + 1";
                    break;
                case 4:
                    stroka = "gren = gren + 1";
                    break;
                case 5:
                    stroka = "gray = gray + 1";
                    break;
            }
            String sql = "UPDATE History SET money = money - " + pubg_case.getPrice() + ", game_case = game_case + 1 , " + stroka + "  WHERE ID_History = 1";
            Cursor c =  myDbHelper.update(sql);
            c.moveToFirst();
            c.close();

            return random_clothesImpl.getPosition();
        }
    }
}
